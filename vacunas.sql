PGDMP                     	    y         
   db_vacunas    13.4    13.4     �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    16840 
   db_vacunas    DATABASE     h   CREATE DATABASE db_vacunas WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'Spanish_Ecuador.1252';
    DROP DATABASE db_vacunas;
                postgres    false            �            1259    19224    empleado    TABLE     '  CREATE TABLE public.empleado (
    id bigint NOT NULL,
    cedula character varying(10) NOT NULL,
    correo character varying(255) NOT NULL,
    direccion character varying(255),
    es_activo boolean NOT NULL,
    es_vacunado boolean NOT NULL,
    fecha_nacimiento timestamp without time zone,
    primer_apellido character varying(255) NOT NULL,
    primer_nombre character varying(255) NOT NULL,
    segundo_apellido character varying(255) NOT NULL,
    segundo_nombre character varying(255) NOT NULL,
    telefono_movil character varying(255)
);
    DROP TABLE public.empleado;
       public         heap    postgres    false            �            1259    19232    empleado_vacuna    TABLE     �   CREATE TABLE public.empleado_vacuna (
    id bigint NOT NULL,
    fecha_vacunacion date,
    numero_dosis integer NOT NULL,
    id_empleado bigint,
    id_tipo_vacuna bigint
);
 #   DROP TABLE public.empleado_vacuna;
       public         heap    postgres    false            �            1259    19257    hibernate_sequence    SEQUENCE     {   CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.hibernate_sequence;
       public          postgres    false            �            1259    19237    rol    TABLE     n   CREATE TABLE public.rol (
    id bigint NOT NULL,
    es_activo boolean,
    nombre character varying(255)
);
    DROP TABLE public.rol;
       public         heap    postgres    false            �            1259    19242    tipo_vacuna    TABLE     v   CREATE TABLE public.tipo_vacuna (
    id bigint NOT NULL,
    es_activo boolean,
    nombre character varying(255)
);
    DROP TABLE public.tipo_vacuna;
       public         heap    postgres    false            �            1259    19247    usuario    TABLE     �   CREATE TABLE public.usuario (
    id bigint NOT NULL,
    es_activo boolean,
    password character varying(255),
    user_name character varying(255),
    id_empleado bigint,
    id_rol bigint
);
    DROP TABLE public.usuario;
       public         heap    postgres    false            �          0    19224    empleado 
   TABLE DATA           �   COPY public.empleado (id, cedula, correo, direccion, es_activo, es_vacunado, fecha_nacimiento, primer_apellido, primer_nombre, segundo_apellido, segundo_nombre, telefono_movil) FROM stdin;
    public          postgres    false    200   �       �          0    19232    empleado_vacuna 
   TABLE DATA           j   COPY public.empleado_vacuna (id, fecha_vacunacion, numero_dosis, id_empleado, id_tipo_vacuna) FROM stdin;
    public          postgres    false    201   )        �          0    19237    rol 
   TABLE DATA           4   COPY public.rol (id, es_activo, nombre) FROM stdin;
    public          postgres    false    202   T        �          0    19242    tipo_vacuna 
   TABLE DATA           <   COPY public.tipo_vacuna (id, es_activo, nombre) FROM stdin;
    public          postgres    false    203   �        �          0    19247    usuario 
   TABLE DATA           Z   COPY public.usuario (id, es_activo, password, user_name, id_empleado, id_rol) FROM stdin;
    public          postgres    false    204   �        �           0    0    hibernate_sequence    SEQUENCE SET     A   SELECT pg_catalog.setval('public.hibernate_sequence', 1, false);
          public          postgres    false    205            5           2606    19231    empleado empleado_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.empleado
    ADD CONSTRAINT empleado_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.empleado DROP CONSTRAINT empleado_pkey;
       public            postgres    false    200            9           2606    19236 $   empleado_vacuna empleado_vacuna_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.empleado_vacuna
    ADD CONSTRAINT empleado_vacuna_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.empleado_vacuna DROP CONSTRAINT empleado_vacuna_pkey;
       public            postgres    false    201            ;           2606    19241    rol rol_pkey 
   CONSTRAINT     J   ALTER TABLE ONLY public.rol
    ADD CONSTRAINT rol_pkey PRIMARY KEY (id);
 6   ALTER TABLE ONLY public.rol DROP CONSTRAINT rol_pkey;
       public            postgres    false    202            =           2606    19246    tipo_vacuna tipo_vacuna_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.tipo_vacuna
    ADD CONSTRAINT tipo_vacuna_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.tipo_vacuna DROP CONSTRAINT tipo_vacuna_pkey;
       public            postgres    false    203            7           2606    19256 %   empleado uk_elgnbqcwg1gv4h713ytod8n0f 
   CONSTRAINT     b   ALTER TABLE ONLY public.empleado
    ADD CONSTRAINT uk_elgnbqcwg1gv4h713ytod8n0f UNIQUE (cedula);
 O   ALTER TABLE ONLY public.empleado DROP CONSTRAINT uk_elgnbqcwg1gv4h713ytod8n0f;
       public            postgres    false    200            ?           2606    19254    usuario usuario_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.usuario DROP CONSTRAINT usuario_pkey;
       public            postgres    false    204            @           2606    19259 +   empleado_vacuna fk6i7gwjsn9uykc1i139g6edpnt    FK CONSTRAINT     �   ALTER TABLE ONLY public.empleado_vacuna
    ADD CONSTRAINT fk6i7gwjsn9uykc1i139g6edpnt FOREIGN KEY (id_empleado) REFERENCES public.empleado(id);
 U   ALTER TABLE ONLY public.empleado_vacuna DROP CONSTRAINT fk6i7gwjsn9uykc1i139g6edpnt;
       public          postgres    false    201    200    2869            B           2606    19269 #   usuario fk8cp9xlcvihvrsry0pj7wrawfc    FK CONSTRAINT     �   ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT fk8cp9xlcvihvrsry0pj7wrawfc FOREIGN KEY (id_empleado) REFERENCES public.empleado(id);
 M   ALTER TABLE ONLY public.usuario DROP CONSTRAINT fk8cp9xlcvihvrsry0pj7wrawfc;
       public          postgres    false    200    2869    204            A           2606    19264 +   empleado_vacuna fkm2mk0qpy5uv1efl61vhb5pqw2    FK CONSTRAINT     �   ALTER TABLE ONLY public.empleado_vacuna
    ADD CONSTRAINT fkm2mk0qpy5uv1efl61vhb5pqw2 FOREIGN KEY (id_tipo_vacuna) REFERENCES public.tipo_vacuna(id);
 U   ALTER TABLE ONLY public.empleado_vacuna DROP CONSTRAINT fkm2mk0qpy5uv1efl61vhb5pqw2;
       public          postgres    false    203    2877    201            C           2606    19274 #   usuario fkmyv3138vvci6kaq3y5kt4cntu    FK CONSTRAINT        ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT fkmyv3138vvci6kaq3y5kt4cntu FOREIGN KEY (id_rol) REFERENCES public.rol(id);
 M   ALTER TABLE ONLY public.usuario DROP CONSTRAINT fkmyv3138vvci6kaq3y5kt4cntu;
       public          postgres    false    2875    202    204            �   �   x�m̱�0����� ��	��&L,Gm�I�K�,<��1.���O��hK[�$}�j�4R����+����&�"����43�@�%Oah)z^f	��<'��j��5��!P������ӏ�����~�����_��&��i�\)��}7&      �      x�3�4202�5 !N4����� /�}      �   &   x�3�,����wt����2��\}|\]��b���� ��t      �   *   x�3�,�����+��S��\F@1Ǫ����Լ��D�=... �e      �   1   x�3�,�442"NWCsK3K3NCNC.#L)SKSN#N#�=... =q
     