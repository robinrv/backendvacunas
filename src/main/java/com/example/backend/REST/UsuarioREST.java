package com.example.backend.REST;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.backend.model.Empleado;
import com.example.backend.model.Rol;
import com.example.backend.model.Usuario;
import com.example.backend.service.UsuarioService;

@RestController

public class UsuarioREST {
	
	@Autowired
	private UsuarioService usuarioService;
	

	
	@RequestMapping(value = "/usuario/", method = RequestMethod.GET)
	public ResponseEntity<List<Usuario>> getAllUsuarios(){
		return ResponseEntity.ok(usuarioService.findAll());
	}
	

	@RequestMapping(value = "/usuario/{id}", method = RequestMethod.GET)
	public ResponseEntity<Optional<Usuario>> getByIdUsuario(@PathVariable Long id){
		return ResponseEntity.ok(usuarioService.findById(id));
	}
	
	@RequestMapping(value = "/usuario/save/", method = RequestMethod.POST,
	        consumes = MediaType.APPLICATION_JSON_VALUE,
	        produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Usuario> saveUsuario(@RequestBody Usuario usuario) {
	
	try {
		Usuario usuarioSave = usuarioService.save(usuario);
		return ResponseEntity.created(new URI("/usuario/save/"+usuarioSave.getId())).body(usuarioSave);
	} catch (Exception e) {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
	}	
	}
	
	

}
