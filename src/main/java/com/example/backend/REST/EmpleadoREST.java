package com.example.backend.REST;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.backend.model.Empleado;
import com.example.backend.service.EmpleadoService;

@RestController
public class EmpleadoREST {

	@Autowired
	private EmpleadoService empleadoService;
	
	
	@RequestMapping(value = "/empleado/", method = RequestMethod.GET)
	public ResponseEntity<List<Empleado>> getAllEmpleados(){
		return ResponseEntity.ok(empleadoService.findAll());
	}
	
	
	@RequestMapping(value = "/empleado/{id}", method = RequestMethod.GET)
	public ResponseEntity<Optional<Empleado>> getByIdEmpleado(@PathVariable Long id){
	return ResponseEntity.ok(empleadoService.findById(id));
	}
	
	
	@RequestMapping(value = "/empleados/vacunados/{status}", method = RequestMethod.GET)
	public ResponseEntity<List<Empleado>> getEmpleadosVacunados(@PathVariable Boolean status){
	return ResponseEntity.ok(empleadoService.findEmpleadosVacunados(status));
	}
	
	
	@RequestMapping(value = "/empleado/save", method = RequestMethod.POST,
	        consumes = MediaType.APPLICATION_JSON_VALUE,
	        produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Empleado> saveEmpleado(@Valid @RequestBody Empleado empleado)   {
	
	try {
		Empleado empleadoSave = empleadoService.save(empleado);
		return ResponseEntity.created(new URI("/empleado/"+empleadoSave.getId())).body(empleadoSave);
	} catch (Exception e) {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
	}
		

	}
	
	

}
