package com.example.backend.REST;

import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.backend.model.TipoVacuna;
import com.example.backend.service.TipoVacunaService;

@RestController
@RequestMapping("/tipovacuna/")
public class TipoVacunaREST {

	@Autowired
	private TipoVacunaService tipoVacunaService;
	
	@GetMapping
	public ResponseEntity<List<TipoVacuna>> getAllTipoVacunas(){
		return ResponseEntity.ok(tipoVacunaService.findAll());
	}
	
	@GetMapping("{id}")
	public ResponseEntity<Optional<TipoVacuna>> getById(@PathVariable Long id){
		return ResponseEntity.ok(tipoVacunaService.findById(id));
	}
	
	
	
}
