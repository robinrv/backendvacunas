package com.example.backend.REST;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.backend.model.EmpleadoVacuna;
import com.example.backend.service.EmpleadoVacunaService;

@RestController
public class EmpleadoVacunaREST {

	@Autowired
	private EmpleadoVacunaService empleadoVacunaService;
	
	
	@RequestMapping(value = "/empleadovacuna/", method = RequestMethod.GET)
	public ResponseEntity<List<EmpleadoVacuna>> getAllEmpleadoVacuna(){
		return ResponseEntity.ok(empleadoVacunaService.findAll());
	}
	
	@RequestMapping(value = "/empleadovacuna/{id}", method = RequestMethod.GET)
	public ResponseEntity<Optional<EmpleadoVacuna>> getByIdEmpleadoVacuna(@PathVariable Long id){
		return	ResponseEntity.ok(empleadoVacunaService.findById(id));
	}
	
	@RequestMapping(value = "/empleadovacuna/fechas/{desde}/{hasta}", method = RequestMethod.GET)
	public ResponseEntity<List<EmpleadoVacuna>> getByEmpleadosVacunaFechas(@PathVariable String  desde,String  hasta){
		return	ResponseEntity.ok(empleadoVacunaService.getEmpleadoPorFechaVacunado(desde,hasta));
	}
	
	@RequestMapping(value = "/empleadovacuan/tipo/{id}", method = RequestMethod.GET)
	public ResponseEntity<List<EmpleadoVacuna>> getByEmpleadosVacunaFechas(@PathVariable Long id){
		return	ResponseEntity.ok(empleadoVacunaService.findByTipoVacuna(id));
	}
	
}
