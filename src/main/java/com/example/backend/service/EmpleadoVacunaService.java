package com.example.backend.service;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.backend.model.EmpleadoVacuna;
import com.example.backend.repository.EmpleadoVacunaRepository;

@Service
public class EmpleadoVacunaService implements EmpleadoVacunaRepository {

	EmpleadoVacunaRepository empleadoVacunaRepository;
	
	
	@Override
	public List<EmpleadoVacuna> findAll() {
		return empleadoVacunaRepository.findAll();
	}
	
	@Override
	public List<EmpleadoVacuna> getEmpleadoPorFechaVacunado(String desde, String hasta) {
		
		return empleadoVacunaRepository.getEmpleadoPorFechaVacunado(desde, hasta);
	}


	@Override
	public List<EmpleadoVacuna> findAll(Sort sort) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<EmpleadoVacuna> findAllById(Iterable<Long> ids) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends EmpleadoVacuna> List<S> saveAll(Iterable<S> entities) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void flush() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <S extends EmpleadoVacuna> S saveAndFlush(S entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends EmpleadoVacuna> List<S> saveAllAndFlush(Iterable<S> entities) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteAllInBatch(Iterable<EmpleadoVacuna> entities) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAllByIdInBatch(Iterable<Long> ids) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAllInBatch() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public EmpleadoVacuna getOne(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EmpleadoVacuna getById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends EmpleadoVacuna> List<S> findAll(Example<S> example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends EmpleadoVacuna> List<S> findAll(Example<S> example, Sort sort) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<EmpleadoVacuna> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends EmpleadoVacuna> S save(S entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<EmpleadoVacuna> findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean existsById(Long id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void deleteById(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(EmpleadoVacuna entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAllById(Iterable<? extends Long> ids) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll(Iterable<? extends EmpleadoVacuna> entities) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <S extends EmpleadoVacuna> Optional<S> findOne(Example<S> example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends EmpleadoVacuna> Page<S> findAll(Example<S> example, Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends EmpleadoVacuna> long count(Example<S> example) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public <S extends EmpleadoVacuna> boolean exists(Example<S> example) {
		// TODO Auto-generated method stub
		return false;
	}



	@Override
	public List<EmpleadoVacuna> findByTipoVacuna(Long idTipoVacuna) {
		// TODO Auto-generated method stub
		return null;
	}



	

	
	


	/*
	@Override
	public List<EmpleadoVacuna> findByFechas(Date desde, Date hasta) {
		
		List<EmpleadoVacuna> vacunados = new ArrayList<EmpleadoVacuna>();
		List<EmpleadoVacuna> filtrados = new ArrayList<EmpleadoVacuna>();
		
		vacunados = empleadoVacunaRepository.findAll();
		
		for (EmpleadoVacuna vacunado : vacunados) {
			
			if(vacunado.getFechaVacunacion().before(desde) && vacunado.getFechaVacunacion().after(hasta)) {
				filtrados.add(vacunado);
			}
		}	
		
		return filtrados;
	}
	
*/
	
}
