package com.example.backend.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table (name="empleado_vacuna")
public class EmpleadoVacuna {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Temporal(TemporalType.DATE)
	@Column(name="fecha_vacunacion")
	private Date fechaVacunacion;
	
	private int numeroDosis;
	
	@ManyToOne
	@JoinColumn(name="id_tipoVacuna")
	private TipoVacuna TipoVacuna;
	
	@OneToOne
	@JoinColumn(name="id_empleado")
	private Empleado Empleado;
	
	public EmpleadoVacuna() {
		super();
	}

	
	public EmpleadoVacuna(Long id, Date fechaVacunacion, int numeroDosis, boolean esUltimaDosis,
			com.example.backend.model.TipoVacuna tipoVacuna, com.example.backend.model.Empleado empleado) {
		super();
		this.id = id;
		this.fechaVacunacion = fechaVacunacion;
		this.numeroDosis = numeroDosis;
		TipoVacuna = tipoVacuna;
		Empleado = empleado;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getFechaVacunacion() {
		return fechaVacunacion;
	}

	public void setFechaVacunacion(Date fechaVacunacion) {
		this.fechaVacunacion = fechaVacunacion;
	}

	public int getNumeroDosis() {
		return numeroDosis;
	}

	public void setNumeroDosis(int numeroDosis) {
		this.numeroDosis = numeroDosis;
	}

	public TipoVacuna getTipoVacuna() {
		return TipoVacuna;
	}

	public void setTipoVacuna(TipoVacuna tipoVacuna) {
		TipoVacuna = tipoVacuna;
	}

	public Empleado getEmpleado() {
		return Empleado;
	}

	public void setEmpleado(Empleado empleado) {
		Empleado = empleado;
	}

	
	
		

}
