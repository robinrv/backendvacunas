package com.example.backend.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Table (name="empleado")
public class Empleado {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@NotNull(message="Cedula requerida.")
	@Size(min = 10, max = 10, message="La cedula debe tener 10 caracteres.")
	@Pattern(regexp = "[0-9]+", message="La cedula solo debe tener números.")
	@Column(unique=true)
	private String cedula;
	
	@NotNull(message="Primer nombre requerido.")
	@Pattern(regexp = "[A-Za-z]+", message="El nombre solo debe tener letras.")
	private String primerNombre;
	
	@NotNull(message="Segundo nombre requerido.")
	@Pattern(regexp = "[A-Za-z]+", message="El nombre solo debe tener letras.")
	private String segundoNombre;
	
	@NotNull(message="Primer apellido requerido.")
	@Pattern(regexp = "[A-Za-z]+", message="El apellido solo debe tener letras.")
	private String primerApellido;
	
	@NotNull(message="Segundo apellido requerido.")
	@Pattern(regexp = "[A-Za-z]+", message="El apellido solo debe tener letras.")
	private String segundoApellido;
	
	@NotNull(message="Correo requerido.")
	@Email
	private String correo;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+5")
	private Date fechaNacimiento;
	
	private String direccion;
	
	private String telefonoMovil;
	
	private boolean esVacunado;
	private boolean esActivo;
	
	
	public Empleado(String cedula, String primerNombre, String segundoNombre, String primerApellido,
			String segundoApellido, String correo, Date fechaNacimiento, String direccion, String telefonoMovil,
			boolean esVacunado, boolean esActivo) {
		super();
		this.cedula = cedula;
		this.primerNombre = primerNombre;
		this.segundoNombre = segundoNombre;
		this.primerApellido = primerApellido;
		this.segundoApellido = segundoApellido;
		this.correo = correo;
		this.fechaNacimiento = fechaNacimiento;
		this.direccion = direccion;
		this.telefonoMovil = telefonoMovil;
		this.esVacunado = esVacunado;
		this.esActivo = esActivo;
	}
	
	public Empleado() {
		super();
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCedula() {
		return cedula;
	}
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	public String getPrimerNombre() {
		return primerNombre;
	}
	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}
	public String getSegundoNombre() {
		return segundoNombre;
	}
	public void setSegundoNombre(String segundoNombre) {
		this.segundoNombre = segundoNombre;
	}
	public String getPrimerApellido() {
		return primerApellido;
	}
	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}
	public String getSegundoApellido() {
		return segundoApellido;
	}
	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefonoMovil() {
		return telefonoMovil;
	}
	public void setTelefonoMovil(String telefonoMovil) {
		this.telefonoMovil = telefonoMovil;
	}
	public boolean isEsVacunado() {
		return esVacunado;
	}
	public void setEsVacunado(boolean esVacunado) {
		this.esVacunado = esVacunado;
	}
	public boolean isEsActivo() {
		return esActivo;
	}
	public void setEsActivo(boolean esActivo) {
		this.esActivo = esActivo;
	}
	
	
	
}
