package com.example.backend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name="tipo_vacuna")
public class TipoVacuna {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String nombre;
	@Column(name="es_activo")
	private boolean esActivo;
		

	public TipoVacuna() {
		super();
	}


	public TipoVacuna(Long id, String nombre, boolean esActivo) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.esActivo = esActivo;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public boolean isEsActivo() {
		return esActivo;
	}


	public void setEsActivo(boolean esActivo) {
		this.esActivo = esActivo;
	}
	

	
	
}
