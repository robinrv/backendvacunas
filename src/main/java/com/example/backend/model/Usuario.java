package com.example.backend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.JoinColumn;

@Entity
@Table (name="Usuario")
public class Usuario {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String userName;
	private String password;
	@Column(name="es_activo")
	private boolean esActivo;

	@OneToOne
	@JoinColumn(name= "id_empleado")
	private Empleado Empleado;
	
	@ManyToOne
	@JoinColumn (name="id_rol")
	private Rol Rol;

	
		
	public Usuario() {
		super();
	}

	public Usuario(Long id, String userName, String password, boolean esActivo,
			com.example.backend.model.Empleado empleado, com.example.backend.model.Rol rol) {
		super();
		this.id = id;
		this.userName = userName;
		this.password = password;
		this.esActivo = esActivo;
		Empleado = empleado;
		Rol = rol;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isEsActivo() {
		return esActivo;
	}

	public void setEsActivo(boolean esActivo) {
		this.esActivo = esActivo;
	}

	public Empleado getEmpleado() {
		return Empleado;
	}

	public void setEmpleado(Empleado empleado) {
		Empleado = empleado;
	}

	public Rol getRol() {
		return Rol;
	}

	public void setRol(Rol rol) {
		Rol = rol;
	}
	

	
	
}
