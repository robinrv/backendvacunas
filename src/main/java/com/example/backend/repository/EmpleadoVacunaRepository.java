package com.example.backend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import com.example.backend.model.EmpleadoVacuna;

public interface EmpleadoVacunaRepository extends JpaRepository<EmpleadoVacuna,Long>{ 

	
	
	@Query(value="SELECT * FROM empleado_vacuna e WHERE e.id_tipo_vacuna = :idTipoVacuna", nativeQuery = true)
	List<EmpleadoVacuna> findByTipoVacuna(@Param("idTipoVacuna") Long  idTipoVacuna);

	@Query(value="SELECT * FROM empleado_vacuna e WHERE e.fecha_vacunacion ?1, and ?2", nativeQuery = true)
	List<EmpleadoVacuna> getEmpleadoPorFechaVacunado(String desde, String hasta);
	
	
	
}
