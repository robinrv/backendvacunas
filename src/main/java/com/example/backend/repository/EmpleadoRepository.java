package com.example.backend.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.backend.model.Empleado;
import com.example.backend.model.EmpleadoVacuna;


public interface EmpleadoRepository extends JpaRepository<Empleado,Long>{

	
	@Query("SELECT u FROM Empleado u WHERE u.esVacunado = :status")
	List<Empleado> findEmpleadosVacunados(
	  @Param("status") Boolean status);


	
}
